package com.fwmagic.hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;

public class Test {
    public static void main(String[] args) throws Exception{
        Configuration config = HBaseConfiguration.create();
        config.set("hbase.zookeeper.quorum", "hd1:2181,hd2:2181,hd3:2181");
        Connection connection = ConnectionFactory.createConnection(config);

        Table table = connection.getTable(TableName.valueOf("mytable"));
        Get get = new Get("r001".getBytes());
        Result result = table.get(get);
        //String str = new String(result.getValue("base_info".getBytes(), "name".getBytes()));
        while (result.advance()) {
            Cell cell = result.current();
            printCell(cell);
        }
    }

    /**
     * 打印
     *
     * @param cell
     */
    private static void printCell(Cell cell) {
        String r = new String(cell.getRowArray(), cell.getRowOffset(), cell.getRowLength());
        String f = new String(cell.getFamilyArray(), cell.getFamilyOffset(), cell.getFamilyLength());
        String q = new String(cell.getQualifierArray(), cell.getQualifierOffset(), cell.getQualifierLength());
        String v = new String(cell.getValueArray(), cell.getValueOffset(), cell.getValueLength());
        String data = String.format("r=%s,f=%s,q=%s,v=%s", r, f, q, v);
        System.out.println(data);
    }

}
